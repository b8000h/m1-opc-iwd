IWD One Page Checkout 官网(https://www.iwdagency.com/extensions/magento-1-one-page-checkout-suite.html#)
最新版为 6.0.8
样式漂亮了，
同时销售手段增加了，提供免费版，但仅支持 PayPal

---


https://github.com/mozgbrasil/magento-iwd-opc
这个照样 state 切换有问题



iwd_opc
=================================

4.2.2

IWD One step/page checkout extension

For more information please visit https://www.iwdagency.com/extensions/one-step-page-checkout.html

*Please note that we are not the developer of this extension. In this repo we only added modman/composer support. We will not provide any support for this repository. If you have any problems on integration, please use the official link mentioned above.*


Installation
------------

Run the following command:

```
composer require kirchbergerknorr/iwd_opc ~4.2.2
```
